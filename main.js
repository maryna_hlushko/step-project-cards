import {
  LoginModal,
  requestHeaders,
  changeDomElements,
} from "./modules/authorization.js";



document.addEventListener("DOMContentLoaded", checkAuthorization);

function checkAuthorization() {
  if (requestHeaders()["Authorization"] === "Bearer null") {
    init();
  } else {
    changeDomElements();
  }
}

function init() {
  document.querySelector(".btn-header").addEventListener("click", (e) => {
    e.preventDefault();
    new LoginModal().render();
  });
}
