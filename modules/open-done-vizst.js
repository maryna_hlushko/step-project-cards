export function openDoneVizit(obj) {     
    

    let today = new Date();
    let nowYear = String(today.getFullYear());
    let nowMonth = String(("0" + (today.getMonth() + 1)).slice(-2));
    let nowDay = String(today.getDate());

    let dateNow = nowYear + "," + nowMonth + "," + nowDay;

    let dateVizst = String(`${obj.date}`.replace(/-/g, ","));
        


    if (dateVizst < dateNow) {    
         
        obj.status = "завершений";
    } 
    else {
         obj.status = "відкритий";
    }
        
}

