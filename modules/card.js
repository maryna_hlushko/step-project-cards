const token = "42860f71-f330-4848-88a3-646685747ab2";
import { ModalForm } from "./class_modal_form.js";
// import { dragNDrop } from "./drag-drop.js";
export class Card {
  constructor(
    fullName,
    doctor,
    goal,
    urgency,
    description,
    date,
    id,
    pressure,
    BMI,
    disease,
    age,
    lastVisit
  ) {
    this.fullName = fullName;
    this.doctor = doctor;
    this.goal = goal;
    this.urgency = urgency;
    this.description = description;

    this.date = date;
    this.id = id;
    this.pressure = pressure;
    this.BMI = BMI;
    this.disease = disease;
    this.age = age;
    this.lastVisit = lastVisit;
    this.container = document.createElement("div");
    this.containerAdditionalInfo = document.createElement("div");
    this.editButton = document.createElement("button");
    this.deleteButton = document.createElement("button");
    this.buttonShowMore = document.createElement("a");
  }

  createElements(parent = document.body.querySelector(".cards-container")) {
    console.log(this.doctor);
    parent.append(this.container);
    this.container.classList.add("card");
    this.container.id = "draggable",

    this.container.classList.add("m-2");
    this.container.style.width = "18rem";

    this.deleteButton.classList.add("image-button-delete");
    this.editButton.classList.add("image-button-edit");
    this.buttonShowMore.classList.add("btn");
    // this.buttonShowMore.dataset.toggle = 'collaps';
    // this.buttonShowMore.dataset.toggle = 'collaps';
    this.buttonShowMore.classList.add("btn-primary");
    this.buttonShowMore.textContent = "Show more";
    this.container.insertAdjacentHTML(
      "beforeend",
      `
        <div class="card-header d-flex justify-content-lg-between position-relative" >
          
          Візит до ${this.doctor + "a"}
          </div>
          `
    );

    this.container.append(this.editButton);
    this.container.append(this.deleteButton);

    this.container.insertAdjacentHTML(
      "beforeend",
      ` <div class="card-body text-center"></div> 
      <h5 class="card-title text-center">ПІБ</h5>
  <p class="card-text text-center title-full-name">${this.fullName}</p>`
    );

    this.container.append(this.buttonShowMore);
    this.deleteButton.addEventListener("click", () => {
      deleteCard(this.id, this.container);
    });

    this.editButton.addEventListener("click", () => {
      new ModalForm(
        this.doctor,
        this.goal,
        this.date,
        this.description,
        this.urgency,
        this.fullName,
        this.id,
        this.age,
        this.pressure,
        this.BMI,
        this.disease
      ).editForm();
      // new CardiologistOptions(this.age, this.pressure, this.BMI, this.disease).editForm
    });

    this.buttonShowMore.addEventListener("click", () => {
      this.containerAdditionalInfo.textContent = "";
      this.container.style.overflow = "visible";
      this.container.style.maxHeight = "none";
      if (this.buttonShowMore.textContent === "Show more") {
        this.buttonShowMore.before(this.containerAdditionalInfo);
        this.buttonShowMore.textContent = "Show less";
      } else {
        this.containerAdditionalInfo.remove();
        this.buttonShowMore.textContent = "Show more";
        this.container.style.overflow = "hidden";
        this.container.style.maxHeight = "185px";
      }

      this.containerAdditionalInfo.insertAdjacentHTML(
        "afterbegin",
        `<h5 class="card-title text-center">Мета візиту</h5>
      <p class="card-text text-center">${this.goal}</p>
      <h5 class="card-title text-center">Дата візиту</h5>
      <p class="card-text text-center">${this.date}</p>
      <h5 class="card-title text-center">Короткий опис візиту</h5>
      <p class="card-text text-center">${this.description}</p>
      <h5 class="card-title text-center">Терміновість візиту</h5>
      <p class="card-text text-center">${this.urgency}</p>`
      );
      if (this.doctor === "кардіолог") {
        this.containerAdditionalInfo.insertAdjacentHTML(
          "beforeend",
          `
        <h5 class="card-title text-center">Нормальний тиск</h5>
        <p class="card-text text-center">${this.pressure}</p>
        <h5 class="card-title text-center">Індекс маси тіла</h5>
        <p class="card-text text-center">${this.BMI}</p>
        <h5 class="card-title text-center">Перенесені хвороби</h5>
        <p class="card-text text-center">${this.disease}</p>
        <h5 class="card-title text-center">Вік</h5>
        <p class="card-text text-center">${this.age}</p>`
        );
      } else if (this.doctor === "стоматолог") {
        this.containerAdditionalInfo.insertAdjacentHTML(
          "beforeend",
          `<h5 class="card-title text-center">Дата останнього візиту</h5>
          <p class="card-text text-center">${this.lastVisit}</p>`
        );
      } else if (this.doctor === "терапевт") {
        this.containerAdditionalInfo.insertAdjacentHTML(
          "beforeend",
          `<h5 class="card-title text-center">Вік</h5>
          <p class="card-text text-center">${this.age}</p>`
        );
      }
    });
    // dragNDrop(this.container)
  }
}

const deleteCard = (id, container) => {
  fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  })
    .then(({ status }) => {
      if (status === 200) {
        container.remove();
      }
    })
    .catch((err) => console.log(err));
};
