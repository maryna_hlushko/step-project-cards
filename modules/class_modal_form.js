// const appointment = document.querySelector('.btn-create')
import { Card } from "./card.js";

export class ModalForm {
  constructor(doctor, goal, date, description, urgency, fullName, id) {
    this.doctor = doctor;
    this.goal = goal;
    this.date = date;
    this.description = description;
    this.urgency = urgency;
    this.fullName = fullName;
    this.id = id;

    this.formWrapper = document.createElement("div");
    this.form = document.createElement("form");
  }

  formCreate() {
    // this.formWrapper.classList.add("modal");
    // this.formWrapper.classList.add("fade");
    // this.formWrapper.classList.add("show");

    this.formWrapper.classList.add("form-visit-wrapper");
    // this.formWrapper.addEventListener('click', ()=>{this.formWrapper.remove()})

    document.body.append(this.formWrapper);
    this.form.classList.add("form-visit");
    this.formWrapper.append(this.form);

    this.form.insertAdjacentHTML(
      "afterbegin",
      `   
       
        <div class="modal-header">
            <h5 class="modal-title">Створити візит</h5>
            <button type="button" class="close bg-secondary form-close" data-dismiss="modal" aria-label="Close" onclick=document.querySelector('.form-visit-wrapper').remove()>
              <span aria-hidden="true" >&times;</span>
            </button>
          </div>
        
<select id="select-doctor" class="form-control mb-2" id="choose-doctor" name="doctor">
    <option selected disabled>Обрати лікаря</option>
    <option value="кардіолог">Кардіолог</option>
    <option value="стоматолог" >Стоматолог</option>
    <option value="терапевт">Терапевт</option>
  </select>

  <label for="goal" class="form-label">Мета візиту</label>
  <div class="input-group">
    <input type="text" class="form-control" id="goal" name="goal">
  </div>

  <label for="date" class="form-label">Дата візиту</label>
  <div class="input-group">
    <input type="date" class="form-control" id="date" name="date">
    </div>

  <label for="description" class="form-label">Короткий опис візиту</label>
  <div class="input-group">
    <textarea class="form-control" aria-label="With textarea" id="description" name="description"></textarea>
  </div>

  <label for="urgency">Терміновість візиту</label>
  <select class="form-control " id="urgency" name="urgency">
      <option selected disabled>Обрати</option>
      <option >звичайна</option>
      <option >пріоритетна</option>
      <option >невідкладна</option>
    </select>
    <label for="Full-name" class="form-label">ПІБ</label>
  <div class="input-group">
    <input type="text" class="form-control" id="Full-name" name="fullName">
  </div>
<div class="additional-options">

</div>
    `
    );
    const selectDoctor = document.querySelector("#select-doctor");
    console.log(selectDoctor);
    selectDoctor.addEventListener("change", (e) => {
      const additionalOptions = document.querySelector(".additional-options");
      additionalOptions.textContent = "";
      if (e.target === selectDoctor) {
        if (e.target.value === "кардіолог") {
          new CardiologistOptions().formCreate();
        } else if (e.target.value === "стоматолог") {
          new DentistOptions().formCreate();
        } else if (e.target.value === "терапевт") {
          new TherapistOptions().formCreate();
        }
      }
    });
    this.formSubmit();
  }

  editForm() {
    // this.formWrapper.classList.add("modal");
    // this.formWrapper.classList.add("fade");
    // this.formWrapper.classList.add("show");

    this.formWrapper.classList.add("form-visit-wrapper");
    // this.formWrapper.addEventListener('click', ()=>{this.formWrapper.remove()})

    document.body.append(this.formWrapper);
    this.form.classList.add("form-visit");
    this.formWrapper.append(this.form);

    this.form.insertAdjacentHTML(
      "afterbegin",
      `   
       
        <div class="modal-header">
            <h5 class="modal-title">Редагувати візит</h5>
            <button type="button" class="close bg-secondary form-close" data-dismiss="modal" aria-label="Close" onclick=document.querySelector('.form-visit-wrapper').remove()>
              <span aria-hidden="true" >&times;</span>
            </button>
          </div>
          <label for="select-doctor" class="form-label">Лікар</label>     
<select id="select-doctor" class="form-control mb-2" id="choose-doctor" name="doctor" value="">
    <option selected>${this.doctor}</option>
   
   
  </select>

  <label for="goal" class="form-label">Мета візиту</label>
  <div class="input-group">
    <input type="text" class="form-control" id="goal" name="goal" value="${this.goal}">
  </div>

  <label for="date" class="form-label">Дата візиту</label>
  <div class="input-group">
    <input type="text" class="form-control" id="date" name="date" value="${this.date}">
    </div>

  <label for="description" class="form-label">Короткий опис візиту</label>
  <div class="input-group">
  <input type="text" class="form-control" id="goal" name="goal" value="${this.description}">
   
  </div>

  <label for="urgency">Терміновість візиту</label>
  <select class="form-control " id="urgency" name="urgency" >
      <option selected disabled>${this.urgency}</option>
      <option >звичайна</option>
      <option >пріоритетна</option>
      <option >невідкладна</option>
    </select>
    <label for="Full-name" class="form-label">ПІБ</label>
  <div class="input-group">
    <input type="text" class="form-control" id="Full-name" name="fullName" value="${this.fullName}">
  </div>
<div class="additional-options">
<input type="submit" class="btn-primary mt-2" id="" >
</div>
    `
    );
    // const selectDoctor = document.querySelector("#select-doctor");
    // if ( selectDoctor.value === "кардіолог"){
    //     new CardiologistOptions().formEdit()
    //     } else if (selectDoctor.value === "стоматолог") {
    //       new DentistOptions().formEdit();
    //     } else if (selectDoctor.value === "терапевт") {
    //       new TherapistOptions(this.age).formEdit();
    //     }

    editedFormSubmit(this.form, this.id, this.formWrapper, this.container);
  }

  formSubmit() {
    this.form.addEventListener("submit", (event) => {
      event.preventDefault();
      const formData = new FormData(this.form);
      console.log(formData);
      const values = Object.fromEntries(formData.entries());
      console.log(values);
      fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify(values),
      })
        .then((response) => response.json())
        .then((response) => {
          const {
            fullName,
            doctor,
            goal,
            urgency,
            description,
            id,
            date,
            pressure,
            BMI,
            disease,
            age,
            lastVisit,
          } = response;
          new Card(
            fullName,
            doctor,
            goal,
            urgency,
            description,
            date,
            id,
            pressure,
            BMI,
            disease,
            age,
            lastVisit
          ).createElements();
        });

      this.formWrapper.remove();
    });
  }

  // editedFormSubmit() {
  //   this.form.addEventListener("submit", (event) => {
  //     event.preventDefault();
  //     const formData = new FormData(this.form);
  //     console.log(formData);
  //     const values = Object.fromEntries(formData.entries());
  //     console.log(values);
  //     fetch(`https://ajax.test-danit.com/api/v2/cards/${this.id}`, {
  //       method: 'PUT',
  // headers: {
  //   'Content-Type': 'application/json',
  //   'Authorization': `Bearer ${token}`
  // },
  // body: JSON.stringify(values),
  //     })
  //       .then((response) => response.json())
  //       .then((response) => {
  //         const { fullName,
  //           doctor,
  //           goal,
  //           urgency,
  //           description,
  //           id,
  //           date,
  //           pressure,
  //           BMI,
  //           disease,
  //           age,
  //           lastVisit } =
  //           response;
  //         new Card(
  //           fullName,
  //           doctor,
  //           goal,
  //           urgency,
  //           description,
  //            date,
  //            id,
  //           pressure,
  //           BMI,
  //           disease,
  //           age,
  //           lastVisit
  //         ).createElements();
  //       });

  //     this.formWrapper.remove();
  //   });
  // }
}

class DentistOptions extends ModalForm {
  constructor(lastVisit) {
    super();
    this.lastVisit = lastVisit;
  }
  formCreate() {
    const additionalOptions = document.querySelector(".additional-options");
    additionalOptions.insertAdjacentHTML(
      "afterbegin",
      `<div class="dantist-options">
               <label for="lastVisitDate" class="form-label">Дата останнього відвідування</label>
               <div class="input-group">
                 <input type="date" class="form-control" id="lastVisitDate" name="lastVisit">
                
                 </div>
                 <input type="submit" class="btn-primary mt-2" id="" >
               `
    );
  }
  // formEdit() {
  //   const additionalOptions = document.querySelector(".additional-options");
  //   additionalOptions.insertAdjacentHTML(
  //     "afterbegin",
  //     `<div class="dantist-options">
  //              <label for="lastVisitDate" class="form-label">Дата останнього відвідування</label>
  //              <div class="input-group">
  //              <input type="text" class="form-control" id="date" name="date" value="${this.lastVisit}">>

  //                </div>
  //                <input type="submit" class="btn-primary mt-2" id="" >
  //              `
  //   );
  // }
}

class TherapistOptions extends ModalForm {
  constructor(age) {
    super();
    this.age = age;
  }
  formCreate() {
    const additionalOptions = document.querySelector(".additional-options");
    additionalOptions.insertAdjacentHTML(
      "afterbegin",
      `   <div class="therapist-options">
              <label for="age" class="form-label">Вік</label>
              <div class="input-group">
                <input type="text" class="form-control" id="age" name="age" >
              </div> 
              
              <input type="submit" class="btn-primary mt-2" id="" ></button>
                      
                    </div>`
    );
  }
  // formEdit() {
  //   const additionalOptions = document.querySelector(".additional-options");
  //   additionalOptions.insertAdjacentHTML(
  //     "afterbegin",
  //     `   <div class="therapist-options">
  //             <label for="age" class="form-label">Вік</label>
  //             <div class="input-group">
  //               <input type="text" class="form-control" id="age" name="age" value=${this.age}>
  //             </div>

  //             <input type="submit" class="btn-primary mt-2" id="" ></button>

  //                   </div>`
  //   );
  // }
}

class CardiologistOptions extends ModalForm {
  constructor(pressure, mass, illness, age) {
    super();
    this.pressure = pressure;
    this.mass = mass;
    this.illness = illness;
    this.age = age;
  }
  formCreate() {
    const additionalOptions = document.querySelector(".additional-options");
    additionalOptions.insertAdjacentHTML(
      "afterbegin",
      ` <div class="cardiologist-options">
      <div class="row">
              <div class="col">
                  <label for="pressure" class="form-label">Звичайний тиск</label>
                  <div class="input-group">
                    <input type="text" class="form-control" id="pressure" name="pressure" >
                  </div>
              </div>
              <div class="col">
                  <label for="BMI" class="form-label">ІМТ</label>
                  <div class="input-group">
                    <input type="text" class="form-control" id="BMI" name="BMI">
                  </div>
              </div>
            </div>
            <label for="disease" class="form-label">Перенесені захворювання</label>
            <div class="input-group">
              <input type="text" class="form-control" id="disease" name="disease">
            </div>
            <label for="age" class="form-label">Вік</label>
            <div class="input-group">
              <input type="text" class="form-control" id="age" name="age">
            </div>
            <input type="submit" class="btn-primary mt-2" id="" ></div>`
    );
  }
  // formEdit() {
  //   const additionalOptions = document.querySelector(".additional-options");
  //   additionalOptions.insertAdjacentHTML(
  //     "afterbegin",
  //     ` <div class="cardiologist-options">
  //     <div class="row">
  //             <div class="col">
  //                 <label for="pressure" class="form-label">Звичайний тиск</label>
  //                 <div class="input-group">
  //                   <input type="text" class="form-control" id="pressure" name="pressure" value="${this.pressure}">
  //                 </div>
  //             </div>
  //             <div class="col">
  //                 <label for="BMI" class="form-label">ІМТ</label>
  //                 <div class="input-group">
  //                   <input type="text" class="form-control" id="BMI" name="BMI" value="${this.BMI}">
  //                 </div>
  //             </div>
  //           </div>
  //           <label for="disease" class="form-label">Перенесені захворювання</label>
  //           <div class="input-group">
  //             <input type="text" class="form-control" id="disease" name="disease" value="${this.disease}">
  //           </div>
  //           <label for="age" class="form-label">Вік</label>
  //           <div class="input-group">
  //             <input type="text" class="form-control" id="age" name="age" value="${this.age}">
  //           </div>
  //           <input type="submit" class="btn-primary mt-2" id="" ></div>`
  //   );
  // }
  closeModal() {
    this.formWrapper.remove();
  }
}

const editedFormSubmit = (form, id, container, card) => {
  form.addEventListener("submit", (event) => {
    event.preventDefault();
    const formData = new FormData(form);
    console.log(formData);
    const values = Object.fromEntries(formData.entries());
    console.log(values);
    fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(values),
    })
      .then((response) => response.json())
      .then((response) => {
        const {
          fullName,
          doctor,
          goal,
          urgency,
          description,
          id,
          date,
          pressure,
          BMI,
          disease,
          age,
          lastVisit,
        } = response;
        new Card(
          fullName,
          doctor,
          goal,
          urgency,
          description,
          date,
          id,
          pressure,
          BMI,
          disease,
          age,
          lastVisit
        ).createElements();
      });
    container.remove();
    card.remove();
  });
};
