import { ModalForm } from "./class_modal_form.js";
import { Card } from "./card.js";
import { filterCards } from "./view-cards.js";
import { openDoneVizit } from "./open-done-vizst.js";
export class LoginModal {
  constructor(authorization) {
    this._loginModal = document.createElement("div");
    this.loginModalBackground = document.createElement("div");
    this.loginModalContainer = document.createElement("div");
    this.loginCloseContainer = document.createElement("div");
    this.loginModalClose = document.createElement("div");
    this.loginForm = document.createElement("form");
    this.inputLogin = document.createElement("input");
    this.inputLogin.type = "login";
    this.inputPassWord = document.createElement("input");
    this.inputPassWord.type = "password";
    this.infoMessage = document.createElement("p");
    this.confirmBtn = document.createElement("button");
    this.authorization = authorization;
  }
  createModal() {
    this._loginModal.classList.add("login-modal");
    this._loginModal.append(this.loginModalBackground);
    this.loginModalBackground.classList.add("login-modal__background");
    this.loginModalBackground.addEventListener(
      "click",
      this.closeModal.bind(this)
    );
    this._loginModal.append(this.loginModalContainer);
    this.loginModalContainer.classList.add("login-modal__container");
    this.loginModalContainer.append(this.loginCloseContainer);
    this.loginCloseContainer.classList.add("login-close__container");
    this.loginCloseContainer.append(this.loginModalClose);
    this.loginModalClose.classList.add("login-modal__close");
    this.loginModalClose.addEventListener("click", this.closeModal.bind(this));
    this.loginModalContainer.append(this.loginForm);
    this.loginForm.classList.add("login-modal__form");
    this.loginForm.insertAdjacentHTML("beforeend", "<label>Логін</label>");
    this.loginForm.append(this.inputLogin);
    this.loginForm.insertAdjacentHTML("beforeend", "<label>Пароль</label>");
    this.loginForm.append(this.inputPassWord);
    this.loginForm.append(this.infoMessage);
    this.infoMessage.classList.add("login-modal__message");
    this.infoMessage.innerText = "";
    this.loginForm.append(this.confirmBtn);
    this.confirmBtn.classList.add(
      "login-modal-btn__confirm",
      "btn",
      "btn-primary",
      "mt-3"
    );
    this.confirmBtn.innerText = "Підтвердити";
    this.confirmBtn.addEventListener("click", (e) => {
      e.preventDefault();
      if (this.inputLogin.value === "" || this.inputPassWord.value === "") {
        this.infoMessage.innerText = "Усі поля мають бути заповнені";
      } else {
        authorization(
          this.inputLogin.value,
          this.inputPassWord.value,
          this.infoMessage,
          this.closeModal.bind(this)
        );
      }
    });
  }
  closeModal() {
    this._loginModal.remove();
  }
  render(container = document.body) {
    this.createModal();
    container.append(this._loginModal);
  }
}

const authorization = async function (
  inputLogin,
  inputPassWord,
  infoMessage,
  callback
) {
  const response = await fetch(
    "https://ajax.test-danit.com/api/v2/cards/login",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: `${inputLogin}`,
        password: `${inputPassWord}`,
      }),
    }
  );
  if (!response.ok) {
    infoMessage.innerText = "Невірний логін або пароль!";
  } else {
    const token = await response.text();
    localStorage.setItem("token", token);
    callback();
    changeDomElements();
  }
};

export function changeDomElements() {
  const loginButton = document.querySelector(".btn-header");
  loginButton.remove();
  const createVisitButton = document.querySelector(".btn-create");
  createVisitButton.classList.toggle("d-flex");
  const notice = document.querySelector(".notice");
  notice.remove();
  const searchForm = document.getElementById("formFilter");
  searchForm.classList.toggle("d-flex");
  cardLoad();
  createVisit();
}

function cardLoad() {
  fetch("https://ajax.test-danit.com/api/v2/cards", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  })
    .then((response) => response.json())
    .then((response) => {
      console.log(response);
      response.forEach((e) => {
        const {
          fullName,
          doctor,
          goal,
          urgency,
          description,
          id,
          date,
          pressure,
          BMI,
          disease,
          age,
          lastVisit,
          status
        } = e;
        console.log(e);
        new Card(
          fullName,
          doctor,
          goal,
          urgency,
          description,
          date,
          id,
          pressure,
          BMI,
          disease,
          age,
          lastVisit,
          status
        ).createElements();
        openDoneVizit(e);
      });
      filterCards(response, "doctor", "status", "urgency")
    });
}

function createVisit() {
  const createVisitButton = document.querySelector(".btn-create");
  createVisitButton.addEventListener("click", (e) => {
    e.preventDefault();
    new ModalForm().formCreate();
  });
}

export function requestHeaders() {
  const token = localStorage.getItem("token");
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  };
  return headers;
}
